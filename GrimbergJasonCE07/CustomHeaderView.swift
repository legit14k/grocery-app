//
//  CustomHeaderView.swift
//  GrimbergJasonCE07
//
//  Created by Jason Grimberg on 9/13/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class CustomHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var totalStores: UILabel!
    @IBOutlet weak var storeTitleLabel: UILabel!
    @IBOutlet weak var headerButton: UIButton!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

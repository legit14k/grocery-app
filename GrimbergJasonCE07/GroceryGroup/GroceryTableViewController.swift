//
//  GroceryTableViewController.swift
//  GrimbergJasonCE07
//
//  Created by Jason Grimberg on 9/12/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class GroceryTableViewController: UITableViewController {

    // Variables to hold numbers
    var numbers1 = 0
    var numbers2 = 0
    
    // Variables to hold strings
    var tempString1 = ""
    var tempString2 = ""
    
    // Custom model to hold the grocery data
    var allGroceries = [Groceries]()
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Edit turns on 'select mode'
        tableView.allowsMultipleSelectionDuringEditing = true
        
        // Register xib
        let headerNib = UINib.init(nibName: "CustomHeader", bundle: nil)
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "header_ID1")
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "cell_ID_need")
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "cell_ID_purchased")
        
        // Get the path to our EmployeeData.json file
        if let path = Bundle.main.path(forResource: "DefaultGroceryList", ofType: ".json") {
            
            // Create URL with path
            let url = URL(fileURLWithPath: path)
            
            do {
                // Create a Data object from our fils's URL
                // After this line executes, our file is in binary format inside the "data" consstant
                let data = try Data.init(contentsOf: url)
                
                // Create a json Object from the binary Data File.
                // Cast it as an array of any type objects.
                let jsonObj = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [Any]
                
                // At this point we have an array of Any objects that represents our JSON data from our file
                // We can now parse through the jsonObj and start instantiating our Customer objects.
                Parse(jsonObject: jsonObj)
            }
            catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func Parse(jsonObject: [Any]?) {
        
        guard let json = jsonObject
            else { print("Parse Failed to Unwrap the Optional"); return }
        
        // Loop through each first level item in the json array
        for firstLevelItem in json {
            // Can these items all be unwrapped? Return if not, otherwise carry on
            guard let object = firstLevelItem as? [String: Any],
                let sName = object["store"] as? String,
                let nItems = object["items_needed"] as? [String],
                let pItems = object["items_purchased"] as? [String]
                else { continue }
//            // Test print to see the data
//            print("Store: \(sName)")
//
//            print("\tNeed: \(nItems.count)")
//            for i in nItems {
//                numbers1 = numbers1 + 1
//                tempString1 += i + " "
//                print("\t\t\(i)")
//            }
//
//            print("\tPurchased: \(pItems.count)")
//            for j in pItems {
//                numbers2 = numbers2 + 1
//                tempString2 += j + " "
//                print("\t\t\(j)")
//            }
            
            // Create our new groceries and append them to the array
            allGroceries.append(Groceries(storeName: sName, numNeeded: nItems.count, numPurchases: pItems.count, needItem: nItems, purchaseItem: pItems))
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return allGroceries.count
    }

    // The height for the header in the section
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 118
    }
    
    // Fire when a multi select row is chosen
    // Catches the WillSelectRowAt callback
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if tableView.isEditing == true {
            
            // For testing purposes(debugging)
            // Uncomment for result
            // print("Selecting row: " + indexPath.row.description)
        }
        // For testing purposes(debugging)
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        if tableView.isEditing == true {
            
            // For testing purposes(debugging)
            // Uncomment for result
            // print("Unselecting row: " + indexPath.row.description)
        }
        // For testing purposes(debugging)
        return indexPath
    }
    
    // Edit button
    @IBAction func editTapped(_ sender: UIBarButtonItem) {
        
        // Turn on/off the editing function
        tableView.setEditing(!tableView.isEditing, animated: true)
        
        if tableView.isEditing {
            // Change the plus to a trash can
            navigationItem.leftBarButtonItem? = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(GroceryTableViewController.trashAllSelected))
            
            // Change the edit button to a cancel button
            navigationItem.rightBarButtonItem? = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(GroceryTableViewController.cancelTrash))
        }
        else {
            // Once tapped again the trash can becomes the add symbol
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(GroceryTableViewController.addTapped(_:)))
        }
    }
    
    // Add button
    @IBAction func addTapped(_ sender: UIBarButtonItem) {
        // Add more stores
        displayTextAlert()
    }
    
    @objc func cancelTrash() {
        // Shut off the editing function
        tableView.setEditing(!tableView.isEditing, animated: true)
        
        // Change the 'cancel' back to edit
        navigationItem.rightBarButtonItem? = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(GroceryTableViewController.editTapped(_:)))
        
        // Change the left bar button back to add
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(GroceryTableViewController.addTapped(_:)))
    }
    
    // Loop through and delete all of the comics and cells that have been selected in Edit Mode
    @objc func trashAllSelected() {
        if var selectedIPs = tableView.indexPathsForSelectedRows {
            // Sort in largets to smallest index so we can remove items from back to front
            selectedIPs.sort { (a, b) -> Bool in
                a.row > b.row
            }
            for indexpath in selectedIPs {
                // Update data (delete the comic) first
                allGroceries.remove(at: indexpath.row)
            }
            // Delete all the rows at once
            tableView.deleteRows(at: selectedIPs, with: .left)
        }
        // Fix the bar buttons
        cancelTrash()
        
        // Update the data in the header and the table view
        tableView.reloadData()
    }
    
    // Add a store
    @IBAction func addStoreTapped (_ sender: UIButton) {
        // Call the test alert
        displayTextAlert()
        
        // Update the TableView
        tableView.reloadData()
    }
    
    // Add new stores when needed
    func addStores(newStore: String) {
        // Call the test alert
        allGroceries.append(Groceries(storeName: newStore, numNeeded: 0, numPurchases: 0, needItem: [], purchaseItem: []))
        
        // Update the TableView
        tableView.reloadData()
    }
    
    // Configure the cells
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)  as! GroceryTableViewCell
        
        // Configure the cell...
        cell.StoreLabel.text = allGroceries[indexPath.row].storeName
        cell.StoreCountLabel.text = allGroceries[indexPath.row].numNeeded.description
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // Create the view
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header_ID1") as? CustomHeaderView
        
        // Configure the view
        header?.storeTitleLabel.text = "All Stores"
        header?.totalStores.text = "Stores: " + allGroceries.count.description
        
        // Return the view
        return header
    }
    
    // Functin to display any errors that would occure
    func displayTextAlert() {
        // Set the text of the alert
        let alertController = UIAlertController(title: "Store?", message: "Please input store name:", preferredStyle: .alert)
        
        // Set the confirm button and make sure there is a text box
        let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
            guard let textFields = alertController.textFields,
                textFields.count > 0 else {
                    // Could not find textfield
                    return
            }
            // Set the number of text fields
            let field = textFields[0]
            
            // Store the new data
            UserDefaults.standard.set(field.text, forKey: "newStore")
            UserDefaults.standard.synchronize()
            
            // Call the add stores and add the new store
            self.addStores(newStore: field.text!)
            
        }
        
        // Set the cancel button
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        // Set a placeholder
        alertController.addTextField { (textField) in
            textField.placeholder = "Home Goods"
        }
        
        // Show both buttons in the alert
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        // Animate the alert controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the selected row
            self.allGroceries.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        // Reload the tableView Data
        tableView.reloadData()
    }
    
    // Unwind from the details VC and update the data from the source
    @IBAction func unwindFromDetails(_ sender: UIStoryboardSegue) {
        if sender.source is DetailsTableViewController {
            if let senderVC = sender.source as? DetailsTableViewController {
                
                if allGroceries.contains(where: { $0.storeName == senderVC.post.storeName }) {
                    // Check to see where the index of the store name that we are editing
                    if let index = allGroceries.index(where: {$0.storeName == senderVC.post.storeName}) {
                        // Remove the store at that current index
                        allGroceries.remove(at: index)
                        // Re-add the store where that index was
                        allGroceries.insert(Groceries(storeName: senderVC.post.storeName, numNeeded: senderVC.post.needItem.count, numPurchases: senderVC.post.purchaseItem.count, needItem: senderVC.post.needItem, purchaseItem: senderVC.post.purchaseItem), at: index)
                    }
                } else {
                    print("Should Not Get to this point")
                }
            }
            // Reload the table view
            tableView.reloadData()
        }
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let indexPath = tableView.indexPathForSelectedRow {
            let postToSend = allGroceries[indexPath.row]
            
            if let destination = segue.destination as? DetailsTableViewController {
                destination.post = postToSend
            }
        }
    }
    
    // Only segue if we are not editing
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if tableView.isEditing != true {
            return true
        }
        return false
    }
}

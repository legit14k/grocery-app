//
//  GroceryTableViewCell.swift
//  GrimbergJasonCE07
//
//  Created by Jason Grimberg on 9/13/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class GroceryTableViewCell: UITableViewCell {

    @IBOutlet weak var StoreLabel: UILabel!
    @IBOutlet weak var StoreCountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

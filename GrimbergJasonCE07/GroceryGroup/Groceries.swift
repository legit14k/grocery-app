//
//  Groceries.swift
//  GrimbergJasonCE07
//
//  Created by Jason Grimberg on 9/13/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import Foundation

class Groceries {
    /* Stored Properites */
    let storeName: String
    let numNeeded: Int
    let numPurchases: Int
    var needItem: [String]
    var purchaseItem: [String]
    
    /* Computed Properties */
    
    /* Initializers */
    init(storeName: String, numNeeded: Int, numPurchases: Int, needItem: [String], purchaseItem: [String]) {
        self.storeName = storeName
        self.numNeeded = numNeeded
        self.numPurchases = numPurchases
        self.needItem = needItem
        self.purchaseItem = purchaseItem
    }
    
    /* Methods */
}

//
//  DetailsTableViewController.swift
//  GrimbergJasonCE07
//
//  Created by Jason Grimberg on 9/15/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class DetailsTableViewController: UITableViewController {

    var post: Groceries!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Register xib
        let headerNib = UINib.init(nibName: "CustomHeader", bundle: nil)
        tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "cell_ID_need")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return post.needItem.count
        } else {
            return post.purchaseItem.count
        }
    }

    // Each cell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier1", for: indexPath)
        
        // Configure the cell...
        if indexPath.section == 0 {
            cell.textLabel?.text = post.needItem[indexPath.row]
        } else {
            cell.textLabel?.text = post.purchaseItem[indexPath.row]
        }
        return cell
    }
 
    // Header for the view in section
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // Create the view
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "cell_ID_need") as? CustomHeaderView
        
        if section == 0 {
            // Configure the view
            header?.headerButton.tag = 0
            header?.storeTitleLabel.text = post.storeName
            header?.totalStores.text = "Needed: " + post.needItem.count.description
        } else {
            // Configure the view
            header?.headerButton.tag = 1
            header?.storeTitleLabel.text = post.storeName
            header?.totalStores.text = "Purchases: " + post.purchaseItem.count.description
        }
        
        // Return the view
        return header
    }

    // The height for the header in the section
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        // Set the current height so we can see all of the cells
        return 118
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            // Add the item to the purchase section
            post.purchaseItem.append(post.needItem[indexPath.row])
            // Remove from the need section
            post.needItem.remove(at: indexPath.row)
        } else {
            // Add the item to the need section
            post.needItem.append(post.purchaseItem[indexPath.row])
            // Remove from the purchase section
            post.purchaseItem.remove(at: indexPath.row)
        }
        // Refresh the data
        tableView.reloadData()
    }
    
    @IBAction func addTapped(_ sender: UIButton) {

        // Display an alert and send the tag number
        displayTextAlert(tagNumber: sender.tag)
    }
    
    // Functin to display any errors that would occure
    func displayTextAlert(tagNumber: Int) {
        // Set the text of the alert
        let alertController = UIAlertController(title: "Item?", message: "Please input item name:", preferredStyle: .alert)
        
        // Set the confirm button and make sure there is a text box
        let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
            guard let textFields = alertController.textFields,
                textFields.count > 0 else {
                    // Could not find textfield
                    return
            }
            // Set the number of text fields
            let field = textFields[0]
            
            // Store the new data
            UserDefaults.standard.set(field.text, forKey: "newItem")
            UserDefaults.standard.synchronize()
            
            // Call the add stores and add the new store
            self.addItems(newItem: field.text!, newTag: tagNumber)
            
        }
        // Set the cancel button
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        // Set a placeholder
        alertController.addTextField { (textField) in
            textField.placeholder = "Yogurt, Eggs, Socks"
        }
        
        // Show both buttons in the alert
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        // Animate the alert controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    // Add new stores when needed
    func addItems(newItem: String, newTag: Int) {
        
        if newTag == 0 {
            // Call the test alert
            post.needItem.append(newItem)
        } else {
            // Call the test alert
            post.purchaseItem.append(newItem)
        }
        
        // Update the TableView
        tableView.reloadData()
    }
    
    // Swipe over to delete
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            if indexPath.section == 0 {
                // Delete the selected row
                self.post.needItem.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            } else {
                // Delete the selected row
                self.post.purchaseItem.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        }
        // Reload the tableView Data
        tableView.reloadData()
    }
 
}
